package main

import (
	"bufio"
	"bytes"
	"encoding/xml"
	"fmt"
	"io"
	"os"
)

func myOut(msg string) int {
	num, err := io.WriteString(os.Stdout, msg)
	if err != nil {
		return -1
	}
	return num
	/*
		f := bufio.NewWriter(os.Stdout)
		defer f.Flush()
		//f.Write(buf)
		num ,err := f.WriteString(msg)
		if (err != nil) {
			return -1
		}
		return num*/
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		if len(scanner.Bytes()) < 1 {
			continue
		}
		// myOut(scanner.Text())
		r := bytes.NewBufferString(scanner.Text())
		decoder := xml.NewDecoder(r)
		inCityElement := false
		for {
			t, err := decoder.Token()
			if err == io.EOF {
				// io.EOF is a successful end
				break
			}
			if err != nil {
				fmt.Printf("decoder.Token() failed with '%s'\n", err)
				break
			}

			switch v := t.(type) {

			case xml.StartElement:
				if v.Name.Local == "person" {
					for _, attr := range v.Attr {
						if attr.Name.Local == "age" {
							fmt.Printf("Element: '<person>', attribute 'age' has value '%s'\n", attr.Value)
						}
					}
				} else if v.Name.Local == "city" {
					inCityElement = true
				}

			case xml.EndElement:
				if v.Name.Local == "city" {
					inCityElement = false
				}

			case xml.CharData:
				if inCityElement {
					fmt.Printf("City: %s\n", string(v))
				}

			case xml.Comment:
				fmt.Printf("Comment: %s\n", string(v))

			case xml.ProcInst:
			// handle XML processing instruction like <?target inst?>

			case xml.Directive:
				// handle XML directive like <!text>
			}
		}

	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}
