package main

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"io"
	"os"
)

func main() {
	// nBytes, nChunks := int64(0), int64(0)
	r := bufio.NewReader(os.Stdin)
	decoder := xml.NewDecoder(r)
	inOrganisationName := false
	for {
		t, err := decoder.Token()
		if err == io.EOF {
			// io.EOF is a successful end
			break
		}
		if err != nil {
			fmt.Printf("decoder.Token() failed with '%s'\n", err)
			break
		}

		switch v := t.(type) {

		case xml.StartElement:
			if v.Name.Local == "OrganisationName" {
				fmt.Println(v)
				fmt.Printf("Element: '<N2:OrganisationName>'")
				inOrganisationName = true
				for _, attr := range v.Attr {
					if attr.Name.Local == "age" {
						fmt.Printf("Element: '<N2:OrganisationName>', attribute 'age' has value '%s'\n", attr.Value)
					}
				}
			} else if v.Name.Local == "N2:NameElement" {
				inOrganisationName = true
			}

		case xml.EndElement:
			if v.Name.Local == "OrganisationName" {
				inOrganisationName = false
			}

		case xml.CharData:
			if inOrganisationName {
				fmt.Printf("OrganisationName: %s\n", string(v))
			}

		case xml.Comment:
			fmt.Printf("Comment: %s\n", string(v))

		case xml.ProcInst:
			// handle XML processing instruction like <?target inst?>

		case xml.Directive:
			// handle XML directive like <!text>
		}
	}
}
